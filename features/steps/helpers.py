from dataclasses import dataclass
from functools import singledispatch
from typing import Any


@dataclass(frozen=True)
class Color:
    """Color Class."""

    name: str


@singledispatch
def full_add(a: Any, b: Any) -> Any:
    """Generic add."""

    raise NotImplementedError()


@full_add.register(int)
def full_add_int(a: int, b: int) -> int:
    """Generic add."""

    return a + b


@full_add.register(Color)
def full_add_color(a: Color, b: Color) -> Color:
    """Generic add."""

    # hack baby hack
    if a.name == "red" or b.name == "red":
        if a.name == "blue" or b.name == "blue":
            return Color("purple")
        else:
            return Color("brown")
    else:
        return Color("brown")
