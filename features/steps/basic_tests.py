"""Basic Tests."""


from behave import given, then, when
from behave.matchers import matcher_mapping, use_step_matcher
from cucumber_expressions.parameter_type import ParameterType
from cucumber_expressions.parameter_type_registry import ParameterTypeRegistry

from cuke4behave.step_matcher import build_step_matcher
from features.steps.helpers import Color, full_add

color_ptr = ParameterType(
    "color",
    "red|blue|orange|purple|brown",
    Color,
    lambda s: Color(s),
    use_for_snippets="",
    prefer_for_regexp_match=False,
)
ptr = ParameterTypeRegistry()
ptr.define_parameter_type(color_ptr)
# partial Cucumber Expression Matcher

step_matcher = build_step_matcher(ptr)

matcher_mapping["cucumber_expressions"] = step_matcher

use_step_matcher("cucumber_expressions")


@given("{int} and {int}")
def step_given(context, int0, int1):
    """Add given."""
    context.addition_args = (int0, int1)


@when("added")
def step_when(context):
    """When they are added."""
    context.add_result = full_add(context.addition_args[0], context.addition_args[1])


@then("there should be {int}")
def step_then_int(context, int0):
    """Then there should be."""
    assert context.add_result == int0


@given("A {color} ball and a {color} ball")
def step_color_given(context, color0, color1):
    """Add given colors."""
    context.addition_args = (color0, color1)


@then("there should be a {color} ball")
def step_then_color(context, color0):
    """Then there should be."""
    assert context.add_result == color0
